package configuration;

public class StubConfiguration implements Configuration {

    boolean shoppingCartSaveFunctionalityEnabled = false;

    @Override
    public boolean isShoppingCartSaveFunctionalityEnabled() {
        return shoppingCartSaveFunctionalityEnabled;
    }

    public void setShoppingCartSaveFunctionalityEnabled(boolean shoppingCartSaveFunctionalityEnabled) {
        this.shoppingCartSaveFunctionalityEnabled = shoppingCartSaveFunctionalityEnabled;
    }
}
