package businesslogic;

import configuration.StubConfiguration;
import model.ShoppingCart;
import org.junit.jupiter.api.Test;
import persistence.ShoppingCartRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestShoppingCartService {


    @Test
    public void testGetSavedShoppingCarts() {
        // Setup saved shopping carts
        List<ShoppingCart> savedShoppingCarts = new ArrayList<>();
        savedShoppingCarts.add(new ShoppingCart());
        savedShoppingCarts.add(new ShoppingCart());

        // Setup shoppingCartRepostiory mock
        ShoppingCartRepository shoppingCartRepostiory = mock(ShoppingCartRepository.class);
        when(shoppingCartRepostiory.getSavedShoppingCarts()).thenReturn(savedShoppingCarts);

        // Setup Configuration
        StubConfiguration configuration = new StubConfiguration();
        configuration.setShoppingCartSaveFunctionalityEnabled(false);

        // Setup CUT (class under test)
        ShoppingCartService shoppingCartService = new ShoppingCartService(configuration, shoppingCartRepostiory);

        // Assertion
        assertTrue(shoppingCartService.getSavedShoppingCarts().isEmpty());
    }
}
