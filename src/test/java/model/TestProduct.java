package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestProduct {

    @Test
    public void testGrossSumCalculation() {
        // Setup
        Product p = new Product();
        p.setName("pizza");
        p.setNetValue(20.0);
        p.setTaxPercentage(0.05);

        assertEquals(21.0, p.getGrossValue(), "Brutto Wert stimmt nicht");
    }

    @Test
    public void testDifficultGrossSumCalculation() {
        Product p = new Product();
        p.setName("Stuff");
        p.setNetValue(3.0);
        p.setTaxPercentage(0.10);
        assertEquals(3.3, p.getGrossValue(), 0.001);
    }
}
