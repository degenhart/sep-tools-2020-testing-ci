package configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileConfiguration implements Configuration {

    private Properties prop;

    public FileConfiguration() {
        try (FileInputStream fis = new FileInputStream("application.properties")) {
            prop = new Properties();
            prop.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isShoppingCartSaveFunctionalityEnabled() {
        return Boolean.parseBoolean(prop.getProperty("ShoppingCartSaveFunctionality"));
    }
}
