package configuration;

public interface Configuration {

    boolean isShoppingCartSaveFunctionalityEnabled();
}
