package businesslogic;

import configuration.Configuration;
import model.ShoppingCart;
import persistence.ShoppingCartRepository;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartService {

    private final Configuration config;

    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartService(Configuration config, ShoppingCartRepository shoppingCartRepository) {
        this.config = config;
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public List<ShoppingCart> getSavedShoppingCarts() {
        if (config.isShoppingCartSaveFunctionalityEnabled()) {
            return shoppingCartRepository.getSavedShoppingCarts();
        } else {
            return new ArrayList<>();
        }
    }
}
